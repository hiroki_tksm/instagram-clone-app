Rails.application.routes.draw do
  root   'static_pages#home'
  get    '/search',           to: 'static_pages#search'
  get    '/terms_of_service', to: 'users#terms_of_service'
  get    '/signup',           to: 'users#new'
  post   '/signup',           to: 'users#create'
  get    '/login',            to: 'sessions#new'
  post   '/login',            to: 'sessions#create'
  delete '/logout',           to: 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers, :likes
    end
  end
  resources :password_resets, only:[:edit, :update]
  resources :posts,           only:[:new,:create, :show, :destroy] do
    resources :comments,      only: [:create, :destroy]
  end
  resources :relationships,   only: [:create, :destroy]
  resources :favorites,       only: [:create, :destroy]
  resources :notifications,   only: :index
end

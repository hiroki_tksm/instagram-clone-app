class AddDetailsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :website, :string
    add_column :users, :introduction, :text
    add_column :users, :telephone, :integer
    add_column :users, :gender, :string
    add_column :users, :password_new, :string
    add_column :users, :password_new_confirmation, :string
  end
end

class ChangeColumnToUser < ActiveRecord::Migration[5.1]

  def change
    change_column :users, :telephone, :integer, :limit =>8
  end
end

$(function(){
$("#modal-open").click( function(){
	$( this ).blur() ;
	if( $( "#modal-overlay" )[0] ) return false ;
	$( "body" ).append( '<div id="modal-overlay"></div>' ) ;
	$( "#modal-overlay" ).fadeIn( "slow" ) ;
	centeringModalSyncer() ;
	$( "#modal-main" ).fadeIn( "slow" ) ;
	$( "#modal-overlay,#modal-close" ).unbind().click( function(){
		$( "#modal-main,#modal-overlay" ).fadeOut( "slow" , function(){
			$('#modal-overlay').remove() ;
		} ) ;
	} ) ;
} ) ;
$( window ).resize( centeringModalSyncer ) ;
	function centeringModalSyncer() {
		var w = $( window ).width() ;
		var h = $( window ).height() ;
		var cw = $( "#modal-main" ).outerWidth();
		var ch = $( "#modal-main" ).outerHeight();
		$( "#modal-main" ).css( {"left": ((w - cw)/2) + "px","top": ((h - ch)/2) + "px"} ) ;
	}
} ) ;

class PostsController < ApplicationController
  before_action :logged_in_user, only: [:new,:create,:destroy]
  before_action :correct_user, only: :destroy

  def new
    @post = current_user.posts.build if logged_in?
  end

  def show
    @post = Post.find_by(id:params[:id])
    @comments = @post.comments
    @comment = Comment.new
  end

  def create
    @user = current_user
    @post = current_user.posts.build(post_params)
    @feed_items = []
    if @post.save
      flash[:success] = "Post created!"
      redirect_to root_url
    else
      render 'static_pages/home'
    end
  end

  def destroy
    @post.destroy
    flash[:success] = "Post deleted"
    redirect_to request.referrer||root_url
  end

  def search
    @posts = Post.search(params[:search])
  end

  private

    def post_params
      params.fetch(:post,{}).permit(:picture)
    end

    def correct_user
      @post = current_user.posts.find_by(id:params[:id])
      redirect_to root_url if @post.nil?
    end
end

class FavoritesController < ApplicationController
  before_action :logged_in_user

  def create
    post = Post.find(params[:post_id])
    current_user.like(post)
    flash[:success] = "投稿をお気に入り登録しました！"
    redirect_back(fallback_location: root_url)
    post.create_notification_favorite(current_user)
  end

  def destroy
    post = Post.find(params[:post_id])
    current_user.unlike(post)
    flash[:success] = "投稿のお気に入り登録を解除しました..."
    redirect_back(fallback_location: root_url)
  end
end

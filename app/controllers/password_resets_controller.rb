class PasswordResetsController < ApplicationController
  before_action :get_user, only:[:edit, :update]

  def new
  end

  def create
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    if params[:user][:password_new].empty?
      @user.errors.add(:password, :blank)
      render 'edit'
    elsif @user.password_new == @user.password_new_confirmation
      @user.update(password: params[:user][:password_new])
      log_in @user
      flash[:success] = "パスワードを更新しました"
      redirect_to @user
    else
      render 'edit'
      end
  end

  private

    def get_user
      @user = User.find_by(user_name: params[:user_name])
    end
end

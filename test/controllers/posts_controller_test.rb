require 'test_helper'
class PostsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @mpost = posts(:orange)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Post.count' do
      post posts_path, params: { post: { content: "Lorem ipsum" } }
    end
    assert_redirected_to login_url
  end
end

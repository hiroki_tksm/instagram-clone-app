require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(name: "First User", email: "user1@example.com",
              password: "foobar", password_confirmation: "foobar")
  end

  test "valid user" do
  assert @user.valid?
 end

 test "name presence" do
   @user.name = " "
   assert_not @user.valid?
 end

 test "email presence" do
   @user.email = " "
   assert_not @user.valid?
 end

 test "email uniqueness" do
   duplicate_user = @user.dup
   duplicate_user.email = @user.email.upcase
   @user.save
   assert_not duplicate_user.valid?
 end

 test "email is downcased" do
   mixed_case_email = "Foo@EXamPle.cOM"
   @user.email = mixed_case_email
   @user.save
   assert_equal mixed_case_email.downcase, @user.reload.email
 end

  test "users follow and unfollow" do
    michael = users(:michael)
    archer  = users(:archer)
    assert_not michael.following?(archer)
    michael.follow(archer)
    assert michael.following?(archer)
    assert archer.followers.include?(michael)
    michael.unfollow(archer)
    assert_not michael.following?(archer)
  end

  test "feed contains correct users posts" do
    michael = users(:michael)
    archer  = users(:archer)
    lana    = users(:lana)
    lana.posts.each do |post_following|
      assert michael.feed.include?(post_following)
    end
    michael.posts.each do |post_self|
      assert michael.feed.include?(post_self)
    end
    archer.posts.each do |post_unfollowed|
      assert_not michael.feed.include?(post_unfollowed)
    end
  end
end

require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "when failed to edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    patch user_path(@user),  params: { user: { name:  "",
                                              email: "foo@invalid",
                                              password:              "foo",
                                              password_confirmation: "bar" } }
  end
  test "uneditable log_in_as other_user" do
    log_in_as(@other_user)
    get edit_user_path(@user)
  end

  test "unpatchable log_in_as other_user" do
    log_in_as(@other_user)
    patch user_path(@user), params: { user: { name: @user.name,
                                            email: @user.email } }
  end
end

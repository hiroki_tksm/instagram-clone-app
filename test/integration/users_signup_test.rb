require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  test "singup by invalid infomation" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params:{user: { name:"",
                                  user_name:"",
                          email: "user@invalid",
                                password: "foo",
               password_confirmation: "bar" } }
    end
    assert_template 'users/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end

  test "signup by valid infomation" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, params: { user: { name:  "Example User",
                                      user_name:"Example User",
                                     email: "user@example.com",
                                     password:      "password",
                          password_confirmation: "password" } }
  end
  follow_redirect!
  assert_not flash.empty?
  assert is_logged_in?
  end
end
